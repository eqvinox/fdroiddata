Categories:Office
License:MIT
Web Site:
Source Code:https://github.com/gituser1357/constellations
Issue Tracker:https://github.com/gituser1357/constellations/issues

Auto Name:Constellations
Summary:Show star charts and data
Description:
Star charts and data of the 88 modern constellations, based on data by
Wikipedia.
.

Repo Type:git
Repo:https://github.com/gituser1357/constellations

Build:2.0.2,10004
    commit=v2.0.2
    subdir=app
    gradle=yes

Build:2.0.3,10005
    commit=v2.0.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.0.3
Current Version Code:10005

