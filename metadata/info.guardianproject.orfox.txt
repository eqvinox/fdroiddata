AntiFeatures:
Categories:Internet
License:MPL2
Web Site:https://dev.guardianproject.info/projects/orfox-private-browser/
Source Code:https://github.com/guardianproject/orfoxfennec
Issue Tracker:https://dev.guardianproject.info/projects/orfox-private-browser/issues
Donate:https://www.torproject.org/donate/donate.html

Summary:TOR Browser
Description:
This is a new privacy-enhanced browser -- based on Mozilla Firefox, configured
by default to work with Orbot: Tor for Android.

You can learn more about the design of Tor Browser on the
[https://www.torproject.org/projects/torbrowser/design/ TOR website]. Official
TOR Browser source repository, which this app relies on, is at
[https://gitweb.torproject.org/tor-browser.git/].
.

Name:OrFox
Repo Type:git
Repo:https://github.com/guardianproject/orfoxfennec

Build:1,1
    commit=dummy
    disable=wait for recipe

Update Check Mode:None
