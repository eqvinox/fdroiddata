Categories:Games
License:GPLv3+
Web Site:https://github.com/TodorBalabanov/FourRowSolitaire/blob/HEAD/README.md
Source Code:https://github.com/TodorBalabanov/FourRowSolitaire
Issue Tracker:https://github.com/TodorBalabanov/FourRowSolitaire/issues

Auto Name:Four Row Solitaire
Summary:Play solitaire like card game
Description:
Solitaire-like card game which is customizable with different sets, backgrounds
and difficulties. All moves can be undone and hints can be invoked if you are
stuck. It supports 1 or 3 card draw and featres statistics and a scoreboard.
The sessions are saved, even when you exit the app. Comparison to other games:

Similarities to FreeCell:

* The Cards in the columns are always visible
* There are four individual cells at the top

Similarities to Solitaire:

* There is a deck and discard pile to draw cards from
* Only Kings may be placed at the top of an empty column
* Cards may be removed from the Ace piles and placed back onto the playing field
* Any number of cards can be moved in one move (as long as they are stacked as in Solitaire

Features unique to this game:

* The four individual cells start with cards in them
* You may only go through the deck twice on draw one and three times on draw three
* The obvious: there are only four columns, not 7 or 8 as in Solitaire and FreeCell, respectively

Note: This app might perform automatic update checks, which might track you.
Also, the F-Droid version is signed with a different key, so updates will not
be installable.
.

Repo Type:git
Repo:https://github.com/TodorBalabanov/FourRowSolitaire

Build:1.2,3
    commit=a6c4f4dcf4a05174e263d2737e6bbea2ee816142
    disable=includes awt

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:3

