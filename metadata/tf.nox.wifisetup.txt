AntiFeatures:
Categories: security
License: Apache2
Web Site:
Source Code: https://github.com/eqvinox/wifisetup/commits/camp2015
Issue Tracker:
Donate:

Summary: Creates a secure Wifi connection entry for CCC Camp 2015
Description:
Creates a Wifi connection entry for Camp 2015, with proper
certificate name and CA checks.
.

Repo Type: git
Repo: https://github.com/eqvinox/wifisetup

Build: 0.18,20150810
    commit=f766dfb30373a022cd131f2e189bf4d21b55a49e
    subdir=android

Update Check Mode: None

