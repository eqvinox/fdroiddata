Categories:Internet
License:GPLv3
Web Site:http://greenaddress.com
Source Code:https://github.com/greenaddress/GreenBits
Issue Tracker:https://github.com/greenaddress/GreenBits/issues

Auto Name:GreenBits
Summary:GreenAddress client
Description:
GreenBits is a Bitcoin Wallet for Android provided by GreenAddress.

The wallet main strengths are security, privacy, easy of use and aims is to provide a
great user experience.

* It supports the payment protocol via both click and qrcode scanning and instant confirmation.
* Supports paper wallet scanning in both WIF and BIP38 format.
* Uses multisignature for improved security and per transaction two factor authentication.

Please note that another GreenAddress app is available for Android: it has more features
available however it consumes more resources and even on recent Android devices it may
not be as good of a user experience as this new Android native version.

You can find it at [[it.greenaddress.cordova]].
.

Repo Type:git
Repo:https://github.com/greenaddress/GreenBits

Build:1.33,33
    commit=1.33
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.36,36
    commit=1.36
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.37,37
    commit=1.37
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.38,38
    commit=1.38
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.39,39
    commit=1.39
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.41,41
    commit=1.41
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.42,42
    commit=1.42
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.43,43
    commit=1.43
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.44,44
    commit=1.44
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.45,45
    commit=1.45
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.46,46
    commit=1.46
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.47,47
    commit=1.47
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Build:1.48,48
    commit=1.48
    subdir=app
    submodules=yes
    gradle=production
    srclibs=AutobahnSW-greenaddress@fb1223083dc6e9dfddc0daad3b76dae277765b51
    rm=app/libs/*.jar
    prebuild=pushd $$AutobahnSW-greenaddress$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$AutobahnSW-greenaddress$$/target/autobahn-android-sw-0.5.2-SNAPSHOT.jar libs/
    scandelete=app/src/main/jniLibs/armeabi/libscrypt.so,app/src/main/jniLibs/mips/libscrypt.so,app/src/main/jniLibs/x86/libscrypt.so,app/scrypt/src
    build=./prepare_fdroid.sh

Maintainer Notes:
scandelete libscrypt.so - it is built by F-Droid via prepare_fdroid.sh,
but it is still in GreenAddress repo for users who don't want to use NDK,
so it needs to be removed before building to pass scanning.
prebuild rm autobahn android jar - as above, still in repo for users who
don't want to be bothered with building Autobahn-SW from source.

* Watch prepare_fdroid.sh for changes on new builds.
.

#Auto Update Mode:None
Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.48
Current Version Code:48

