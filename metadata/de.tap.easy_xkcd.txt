Categories:Reading
License:Apache2
Web Site:
Source Code:https://github.com/T-Rex96/Easy_xkcd
Issue Tracker:https://github.com/T-Rex96/Easy_xkcd/issues

Auto Name:Easy xkcd
Summary:View xkcd comics
Description:
A fast and beautiful way to view your favorite [https://xkcd.com/ xkcd] comics.

Features:

– Quick navigation between comics
– Long press image to view alt text
– Search for title, transcript or number
– Share comic url or image
– Add comic to favorites
– Favorites are saved for offline use
– Open links from xkcd.com and m.xkcd.com
– Support for large images (e.g comic 657)
– Warning for interactive comics
– Explain xkcd integration
– Material design elements like Snackbars, Floating Action Button, tinted Status Bar…
.

Repo Type:git
Repo:https://github.com/T-Rex96/Easy_xkcd

Build:1.2.3,10
    commit=581f45e473a1993500887d0aeaceb6a559be8d88
    subdir=app
    gradle=yes

Build:1.2.5,12
    commit=78ff7e8f281193198b0178660108c97703e98147
    subdir=app
    gradle=yes

Build:1.3,13
    commit=695d48c128450b6811ca742b667d74a9d370f9b7
    subdir=app
    gradle=yes

Build:1.4.1,15
    commit=28bcd0f3f437779816f45aa85389afc28f45eb39
    subdir=app
    gradle=yes

Build:1.4.2,16
    commit=f6c5838407e7964aa9832e291be7935297d3e00e
    subdir=app
    gradle=yes

Build:1.4.3,17
    commit=8f1afd0fe4c90ae818f866b025bfb8d909739c51
    subdir=app
    gradle=yes

Build:1.4.4,18
    commit=9fb625c4845f11289f07c90e0a9de7f3e63b2775
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Update Check Name:com.tap.xkcd_reader
Current Version:1.4.4
Current Version Code:18

