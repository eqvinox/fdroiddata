Categories:Office
License:GPLv3+
Web Site:https://github.com/fr3ts0n/AndrOBD/wiki
Source Code:https://github.com/fr3ts0n/AndrOBD
Issue Tracker:https://github.com/fr3ts0n/AndrOBD/issues

Auto Name:AndrOBD
Summary:Connect to your car's OBD system
Description:
Use your phone/tablet to connect to your car's on-board diagnostics system
via any ELM327 bluetooth adapter.
.

Repo Type:git
Repo:https://github.com/fr3ts0n/AndrOBD

Build:V1.0.0,1
    commit=c53e55b53a09fba3cb7de7adab8fc297eca29ae2
    srclibs=android-logging-log4j@83,log4j@1567108,AChartEngineFr3ts0n@e2594a4a257cfeacba3595f1b16218a5541810c3
    rm=libs/*.jar,src/com/fr3ts0n/pvs/gui/,src/com/fr3ts0n/ecu/gui/application/,src/com/fr3ts0n/ecu/prot/Kw1281Prot.java,src/com/fr3ts0n/prot/gui/
    prebuild=pushd $$android-logging-log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$AChartEngineFr3ts0n$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$android-logging-log4j$$/target/android-logging-log4j-1.0.4-SNAPSHOT.jar libs/ && \
        cp $$log4j$$/target/log4j-1.2.18-SNAPSHOT.jar libs/ && \
        cp $$AChartEngineFr3ts0n$$/target/achartengine-1.2.0.jar libs/

Build:V1.1.0,10100
    commit=V1.1.0
    srclibs=android-logging-log4j@83,log4j@1567108,AChartEngineFr3ts0n@e2594a4a257cfeacba3595f1b16218a5541810c3
    rm=libs/*.jar,src/com/fr3ts0n/pvs/gui/,src/com/fr3ts0n/ecu/gui/application/,src/com/fr3ts0n/ecu/prot/Kw1281Prot.java,src/com/fr3ts0n/prot/gui/
    prebuild=pushd $$android-logging-log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$AChartEngineFr3ts0n$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$android-logging-log4j$$/target/android-logging-log4j-1.0.4-SNAPSHOT.jar libs/ && \
        cp $$log4j$$/target/log4j-1.2.18-SNAPSHOT.jar libs/ && \
        cp $$AChartEngineFr3ts0n$$/target/achartengine-1.2.0.jar libs/

Build:V1.2.0,10201
    commit=V1.2.0
    srclibs=android-logging-log4j@83,log4j@1567108,AChartEngineFr3ts0n@e2594a4a257cfeacba3595f1b16218a5541810c3
    rm=libs/*.jar,src/com/fr3ts0n/pvs/gui/,src/com/fr3ts0n/ecu/gui/application/,src/com/fr3ts0n/ecu/prot/Kw1281Prot.java,src/com/fr3ts0n/prot/gui/
    prebuild=pushd $$android-logging-log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$AChartEngineFr3ts0n$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$android-logging-log4j$$/target/android-logging-log4j-1.0.4-SNAPSHOT.jar libs/ && \
        cp $$log4j$$/target/log4j-1.2.18-SNAPSHOT.jar libs/ && \
        cp $$AChartEngineFr3ts0n$$/target/achartengine-1.2.0.jar libs/

Build:V1.2.2,10202
    commit=V1.2.2
    srclibs=android-logging-log4j@83,log4j@1567108,AChartEngineFr3ts0n@e2594a4a257cfeacba3595f1b16218a5541810c3
    rm=libs/*.jar,src/com/fr3ts0n/pvs/gui/,src/com/fr3ts0n/ecu/gui/application/,src/com/fr3ts0n/ecu/prot/Kw1281Prot.java,src/com/fr3ts0n/prot/gui/
    prebuild=pushd $$android-logging-log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$AChartEngineFr3ts0n$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$android-logging-log4j$$/target/android-logging-log4j-1.0.4-SNAPSHOT.jar libs/ && \
        cp $$log4j$$/target/log4j-1.2.18-SNAPSHOT.jar libs/ && \
        cp $$AChartEngineFr3ts0n$$/target/achartengine-1.2.0.jar libs/

Build:V1.2.3,10203
    commit=V1.2.3
    srclibs=android-logging-log4j@83,log4j@1567108,AChartEngineFr3ts0n@e2594a4a257cfeacba3595f1b16218a5541810c3
    rm=libs/*.jar,src/com/fr3ts0n/pvs/gui/,src/com/fr3ts0n/ecu/gui/application/,src/com/fr3ts0n/ecu/prot/Kw1281Prot.java,src/com/fr3ts0n/prot/gui/
    prebuild=pushd $$android-logging-log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$log4j$$ && \
        $$MVN3$$ package && \
        popd && \
        pushd $$AChartEngineFr3ts0n$$ && \
        $$MVN3$$ package && \
        popd && \
        cp $$android-logging-log4j$$/target/android-logging-log4j-1.0.4-SNAPSHOT.jar libs/ && \
        cp $$log4j$$/target/log4j-1.2.18-SNAPSHOT.jar libs/ && \
        cp $$AChartEngineFr3ts0n$$/target/achartengine-1.2.0.jar libs/

Auto Update Mode:None
Update Check Mode:Tags
Current Version:V1.2.3
Current Version Code:10203

