F-Droid Data
============

[![Build status](https://ci.gitlab.com/projects/5274/status.png?ref=master)](https://ci.gitlab.com/projects/5274?ref=master)

This repository holds general and build information for all the apps on our
main repo on f-droid.org.

Quickstart
----------

First, you need [fdroidserver](https://gitlab.com/fdroid/fdroidserver). Once
you have `fdroid` installed, you can start using it.

For most setups, an empty config.py will be sufficient - `touch config.py`.

Contributing
------------

See the [contributing guidelines](CONTRIBUTING.md).

More information
----------------

You can find more details on [the manual](https://f-droid.org/manual/).
